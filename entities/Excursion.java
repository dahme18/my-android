package com.example.d308mobiledevelopmentapplicationandroid.entities;


import androidx.room.Entity;
import androidx.room.PrimaryKey;
/**
 * Project: Vacation Scheduler
 * Package: com.example.d308mobiledevelopmentapplicationandroid.Dao
 * <p>
 * User: Dawood Ahmed
 * Date: 6/14/2023
 * Time: 4:04 PM
 * <p>
 * Created with Android Studio
 * To change this template use File | Settings | File Templates.
 */

@Entity(tableName = "Excursions")
public class Excursion {
    @PrimaryKey(autoGenerate = true)
    private int excursionID;

    private int vacationID;

    private String excursionTitle;
    private String excursionDate;

    public Excursion(int excursionID, String excursionTitle, String excursionDate, int vacationID) {
        this.excursionID = excursionID;
        this.excursionTitle= excursionTitle;
        this.excursionDate = excursionDate;
        this.vacationID = vacationID;
    }

    public String getExcursionDate() {
        return excursionDate;
    }

    public void setExcursionDate(String excursionDate) {
        this.excursionDate = excursionDate;
    }

    public Excursion() {
    }


    public int getExcursionID() {
        return excursionID;
    }

    public void setExcursionID(int excursionID) {
        this.excursionID = excursionID;
    }

    public String getExcursionTitle() {
        return excursionTitle;
    }

    public void setExcursionTitle(String excursionTitle) {
        this.excursionTitle = excursionTitle;
    }

    public String getDate() {
        return excursionDate;
    }

    public void setDate(String date) {
        this.excursionDate = date;
    }

    public int getVacationID() {
        return vacationID;
    }

    public void setVacationID(int vacationID) {
        this.vacationID = vacationID;
    }


}

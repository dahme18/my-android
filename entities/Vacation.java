package com.example.d308mobiledevelopmentapplicationandroid.entities;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Project: Vacation Scheduler
 * Package: com.example.d308mobiledevelopmentapplicationandroid.Dao
 * <p>
 * User: Dawood Ahmed
 * Date: 6/14/2023
 * Time: 4:04 PM
 * <p>
 * Created with Android Studio
 * To change this template use File | Settings | File Templates.
 */
@Entity(tableName = "Vacations")
public class Vacation {
    @PrimaryKey(autoGenerate = true)
    private int vacationID;
    private String vacationTitle;
    private String vacationHotel;
    private String vacationStartDate;
    private  String vacationEndDate;

    public Vacation(int vacationID, String vacationTitle , String vacationHotel,String vacationStartDate, String vacationEndDate) {
        this.vacationID = vacationID;
        this.vacationTitle = vacationTitle ;
        this.vacationHotel = vacationHotel;
        this.vacationStartDate = vacationStartDate;
        this.vacationEndDate = vacationEndDate;

    }
    public Vacation (){

    }

    public int getVacationID() {return vacationID;}
    public void setVacationID(int vacationID){
        this.vacationID = vacationID;
    }
    public String getVacationTitle() { return vacationTitle; }
    public void setVacationTitle(String vacationTitle) { this.vacationTitle = vacationTitle;}

    public String getVacationHotel() { return vacationHotel; }
    public void setVacationHotel(String vacationHotel) { this.vacationHotel = vacationHotel;}

    public String getVacationStartDate() { return vacationStartDate; }
    public void setVacationStartDate(String vacationStartDate) { this.vacationStartDate = vacationStartDate;}

    public String getVacationEndDate() { return vacationEndDate; }
    public void setVacationEndDate(String vacationEndDate) { this.vacationEndDate = vacationEndDate;}

    @NonNull
    @Override
    public String toString() {
        return "Vacation{" +
                "vacationID=" + vacationID +
                ", vacationTitle='" +vacationTitle + '\'' +
                ", vacationHotel='" + vacationHotel + '\'' +
                ",vacationStartDate='" + vacationStartDate + '\'' +
                ",vacationEndDat=" + vacationEndDate +
                '}';
    }

}

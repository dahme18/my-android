package com.example.d308mobiledevelopmentapplicationandroid.UI;


import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.d308mobiledevelopmentapplicationandroid.R;
import com.example.d308mobiledevelopmentapplicationandroid.Database.Repository;
import com.example.d308mobiledevelopmentapplicationandroid.entities.Excursion;
import com.example.d308mobiledevelopmentapplicationandroid.entities.Vacation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Project: Vacation Scheduler
 * Package: com.example.d308mobiledevelopmentapplicationandroid.Dao
 * <p>
 * User: Dawood Ahmed
 * Date: 6/14/2023
 * Time: 4:04 PM
 * <p>
 * Created with Android Studio
 * To change this template use File | Settings | File Templates.
 */
public class ExcursionDetails extends AppCompatActivity {
    EditText editTitle;
    EditText editDate;
    EditText editNote;
    DatePickerDialog.OnDateSetListener startDate;
    final Calendar myCalendarStart = Calendar.getInstance();
    String title;
    String date;
    int id;
    int vacationId;
    Excursion excursion;
    Repository repository;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_excursion_details);
        editTitle = findViewById(R.id.edittitle);
        editDate = findViewById(R.id.editdate);
        editNote = findViewById(R.id.editnote);
        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        editDate.setText(sdf.format(new Date()));
        id = getIntent().getIntExtra("id", -1);
        title = getIntent().getStringExtra("title");
        date = getIntent().getStringExtra("date");
        vacationId = getIntent().getIntExtra("vacationID", -1);
        editTitle.setText(title);
        editDate.setText(date);
        repository = new Repository(getApplication());
        Spinner spinner = findViewById(R.id.spinner);
        ArrayAdapter<Vacation> vacationArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, repository.getAllVacations());
        spinner.setAdapter(vacationArrayAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                editNote.setText(vacationArrayAdapter.getItem(i).toString());
                editNote.setText(vacationArrayAdapter.getItem(i).getVacationTitle().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                editNote.setText("Nothing Selected");
            }
        });

        Button button = findViewById(R.id.saveExcursion);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (id == -1) {
                    Toast.makeText(ExcursionDetails.this,"Excursion Saved Successfully",Toast.LENGTH_LONG).show();
                    excursion = new Excursion(0, editTitle.getText().toString(), editDate.getText().toString(), vacationId);
                    repository.insert(excursion);

                } else {
                    Toast.makeText(ExcursionDetails.this,"Excursion Saved Successfully",Toast.LENGTH_LONG).show();
                    excursion = new Excursion(id, editTitle.getText().toString(), editDate.getText().toString(), vacationId);
                    repository.update(excursion);

                }
            }
        });
        editDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Date date;
                //get value from other screen,but I'm going to hard code it right now
                String info = editDate.getText().toString();
                if (info.equals("")) info = "06/14/2023";
                try {
                    myCalendarStart.setTime(sdf.parse(info));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                new DatePickerDialog(ExcursionDetails.this, startDate, myCalendarStart
                        .get(Calendar.YEAR), myCalendarStart.get(Calendar.MONTH),
                        myCalendarStart.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        startDate = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub

                myCalendarStart.set(Calendar.YEAR, year);
                myCalendarStart.set(Calendar.MONTH, monthOfYear);
                myCalendarStart.set(Calendar.DAY_OF_MONTH, dayOfMonth);


                updateLabelStart();
            }

        };
    }

    private void updateLabelStart() {
        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        editDate.setText(sdf.format(myCalendarStart.getTime()));
    }


    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_excursiondetails, menu);
        return true;
    }




}
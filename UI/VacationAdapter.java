package com.example.d308mobiledevelopmentapplicationandroid.UI;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.d308mobiledevelopmentapplicationandroid.R;
import com.example.d308mobiledevelopmentapplicationandroid.entities.Vacation;

import java.util.List;

/**
 * Project: Vacation Scheduler
 * Package: com.example.d308mobiledevelopmentapplicationandroid.Dao
 * <p>
 * User: Dawood Ahmed
 * Date: 6/14/2023
 * Time: 4:04 PM
 * <p>
 * Created with Android Studio
 * To change this template use File | Settings | File Templates.
 */
public class VacationAdapter extends RecyclerView.Adapter<VacationAdapter.VacationViewHolder> {
    class VacationViewHolder extends RecyclerView.ViewHolder {
        private final TextView vacationItemView;
        private  VacationViewHolder(View itemview){
            super(itemview);
            vacationItemView=itemview.findViewById(R.id.textView2);
            itemview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position=getAdapterPosition();
                    final Vacation current=mVacations.get(position);
                    Intent intent= new Intent(context,VacationDetails.class);
                    intent.putExtra("id", current.getVacationID());
                    intent.putExtra("title", current.getVacationTitle());
                    intent.putExtra("hotel", current.getVacationHotel());
                    intent.putExtra("startDate", current.getVacationStartDate());
                    intent.putExtra("endDate", current.getVacationEndDate());
                    context.startActivity(intent);
                }
            });
        }
    }
    private List<Vacation> mVacations;
    private final Context context;
    private final LayoutInflater mInflater;
    public VacationAdapter(Context context){
        mInflater= LayoutInflater.from(context);
        this.context=context;
    }
    @Override
    public VacationAdapter.VacationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView=mInflater.inflate(R.layout.vacation_list_item,parent,false);
        return new VacationViewHolder((itemView));
    }

    @Override
    public void onBindViewHolder(@NonNull VacationAdapter.VacationViewHolder holder, int position) {
        if(mVacations!=null){
            Vacation current=mVacations.get(position);
            String name=current.getVacationTitle();
            holder.vacationItemView.setText(name);
        }
        else{
            holder.vacationItemView.setText("No Vacation Title");
        }

    }

    @Override
    public int getItemCount() {
        return mVacations.size();
    }

    public void setVacations(List<Vacation> vacations){
        mVacations=vacations;
        notifyDataSetChanged();
    }
}

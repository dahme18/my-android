package com.example.d308mobiledevelopmentapplicationandroid.UI;


import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.d308mobiledevelopmentapplicationandroid.Database.Repository;
import com.example.d308mobiledevelopmentapplicationandroid.R;
import com.example.d308mobiledevelopmentapplicationandroid.entities.Excursion;
import com.example.d308mobiledevelopmentapplicationandroid.entities.Vacation;

/**
 * Project: Vacation Scheduler
 * Package: com.example.d308mobiledevelopmentapplicationandroid.Dao
 * <p>
 * User: Dawood Ahmed
 * Date: 6/14/2023
 * Time: 4:04 PM
 * <p>
 * Created with Android Studio
 * To change this template use File | Settings | File Templates.
 */
public class MainActivity extends AppCompatActivity {
    //First Comment
    public static int numAlert;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button button=findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this, VacationList.class);
                startActivity(intent);
            }
        });

    }
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.addSampleData:
                Vacation vacation = new Vacation(0, "Bermuda Trip", "4 star hotel","06/11/2023", "06/20/2023" );
                Repository repository = new Repository(getApplication());
                repository.insert(vacation);
                Excursion excursion = new Excursion(0, "Swimming", "06/11/2023", 0);
                repository.insert(excursion);
                Toast.makeText(MainActivity.this,"Sample Data Saved Successfully",Toast.LENGTH_LONG).show();

                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
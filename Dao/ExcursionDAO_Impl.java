package com.example.d308mobiledevelopmentapplicationandroid.Dao;

import android.database.Cursor;

import androidx.room.EntityDeletionOrUpdateAdapter;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;

import com.example.d308mobiledevelopmentapplicationandroid.entities.Excursion;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SuppressWarnings({"unchecked", "deprecation"})
public final class ExcursionDAO_Impl implements ExcursionDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter<Excursion> __insertionAdapterOfExcursion;

  private final EntityDeletionOrUpdateAdapter<Excursion> __deletionAdapterOfExcursion;

  private final EntityDeletionOrUpdateAdapter<Excursion> __updateAdapterOfExcursion;

  public ExcursionDAO_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfExcursion = new EntityInsertionAdapter<Excursion>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR IGNORE INTO `parts` (`partID`,`partName`,`price`,`productID`) VALUES (nullif(?, 0),?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, Excursion value) {
        stmt.bindLong(1, value.getExcursionID());
        if (value.getExcursionTitle() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getExcursionTitle());
        }
        stmt.bindString(3, value.getDate());
        stmt.bindLong(4, value.getVacationID());
      }
    };
    this.__deletionAdapterOfExcursion = new EntityDeletionOrUpdateAdapter<Excursion>(__db) {
      @Override
      public String createQuery() {
        return "DELETE FROM `parts` WHERE `partID` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, Excursion value) {
        stmt.bindLong(1, value.getExcursionID());
      }
    };
    this.__updateAdapterOfExcursion = new EntityDeletionOrUpdateAdapter<Excursion>(__db) {
      @Override
      public String createQuery() {
        return "UPDATE OR ABORT `parts` SET `partID` = ?,`partName` = ?,`price` = ?,`productID` = ? WHERE `partID` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, Excursion value) {
        stmt.bindLong(1, value.getExcursionID());
        if (value.getExcursionTitle() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getExcursionTitle());
        }
        stmt.bindString(3, value.getDate());
        stmt.bindLong(4, value.getVacationID());
        stmt.bindLong(5, value.getExcursionID());
      }
    };
  }

  @Override
  public void insert(final Excursion excursion) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __insertionAdapterOfExcursion.insert(excursion);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void delete(final Excursion excursion) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __deletionAdapterOfExcursion.handle(excursion);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void update(final Excursion excursion) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __updateAdapterOfExcursion.handle(excursion);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public List<Excursion> getAllExcursions() {
    final String _sql = "SELECT * FROM PARTS ORDER BY partID ASC";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfExcursionID = CursorUtil.getColumnIndexOrThrow(_cursor, "partID");
      final int _cursorIndexOfExcursionTitle = CursorUtil.getColumnIndexOrThrow(_cursor, "partName");
      final int _cursorIndexOfDate = CursorUtil.getColumnIndexOrThrow(_cursor, "price");
      final int _cursorIndexOfVacationID = CursorUtil.getColumnIndexOrThrow(_cursor, "productID");
      final List<Excursion> _result = new ArrayList<Excursion>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final Excursion _item;
        _item = new Excursion();
        final int _tmpExcursionID;
        _tmpExcursionID = _cursor.getInt(_cursorIndexOfExcursionID);
        _item.setExcursionID(_tmpExcursionID);
        final String _tmpExcursionTitle;
        if (_cursor.isNull(_cursorIndexOfExcursionTitle)) {
          _tmpExcursionTitle = null;
        } else {
          _tmpExcursionTitle = _cursor.getString(_cursorIndexOfExcursionTitle);
        }
        _item.setExcursionTitle(_tmpExcursionTitle);
        final String _tmpDate;
        _tmpDate = _cursor.getString(_cursorIndexOfDate);
        _item.setDate(_tmpDate);
        final int _tmpVacationID;
        _tmpVacationID = _cursor.getInt(_cursorIndexOfVacationID);
        _item.setVacationID(_tmpVacationID);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public List<Excursion> getAllAssociatedExcursions(int vacationID) {
    return null;
  }

  @Override
  public List<Excursion> getAllAssociatadExcursions(final int vacationID) {
    final String _sql = "SELECT * FROM PARTS WHERE productID= ? ORDER BY partID ASC";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    _statement.bindLong(_argIndex, vacationID);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfExcursionID = CursorUtil.getColumnIndexOrThrow(_cursor, "partID");
      final int _cursorIndexOfExcursionTitle = CursorUtil.getColumnIndexOrThrow(_cursor, "partName");
      final int _cursorIndexOfDate = CursorUtil.getColumnIndexOrThrow(_cursor, "price");
      final int _cursorIndexOfVacationID = CursorUtil.getColumnIndexOrThrow(_cursor, "productID");
      final List<Excursion> _result = new ArrayList<Excursion>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final Excursion _item;
        _item = new Excursion();
        final int _tmpExcursionID;
        _tmpExcursionID = _cursor.getInt(_cursorIndexOfExcursionID);
        _item.setExcursionID(_tmpExcursionID);
        final String _tmpExcursionTitle;
        if (_cursor.isNull(_cursorIndexOfExcursionTitle)) {
          _tmpExcursionTitle = null;
        } else {
          _tmpExcursionTitle = _cursor.getString(_cursorIndexOfExcursionTitle);
        }
        _item.setExcursionTitle(_tmpExcursionTitle);
        final String _tmpDate;
        _tmpDate = _cursor.getString(_cursorIndexOfDate);
        _item.setDate(_tmpDate);
        final int _tmpVacationID;
        _tmpVacationID = _cursor.getInt(_cursorIndexOfVacationID);
        _item.setVacationID(_tmpVacationID);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  public static List<Class<?>> getRequiredConverters() {
    return Collections.emptyList();
  }
}

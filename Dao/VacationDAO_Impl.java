package com.example.d308mobiledevelopmentapplicationandroid.Dao;

import android.database.Cursor;

import androidx.room.EntityDeletionOrUpdateAdapter;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;

import com.example.d308mobiledevelopmentapplicationandroid.entities.Vacation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SuppressWarnings({"unchecked", "deprecation"})
public final class VacationDAO_Impl implements VacationDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter<Vacation> __insertionAdapterOfVacation;

  private final EntityDeletionOrUpdateAdapter<Vacation> __deletionAdapterOfVacation;

  private final EntityDeletionOrUpdateAdapter<Vacation> __updateAdapterOfVacation;

  public VacationDAO_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfVacation = new EntityInsertionAdapter<Vacation>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR IGNORE INTO `products` (`productID`,`productName`,`productPrice`) VALUES (nullif(?, 0),?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, Vacation value) {
        stmt.bindLong(1, value.getVacationID());
        if (value.getVacationTitle() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getVacationTitle());
        }
        stmt.bindString(3, value.getVacationHotel());
      }
    };
    this.__deletionAdapterOfVacation = new EntityDeletionOrUpdateAdapter<Vacation>(__db) {
      @Override
      public String createQuery() {
        return "DELETE FROM `vacation` WHERE `vacationID` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, Vacation value) {
        stmt.bindLong(1, value.getVacationID());
      }
    };
    this.__updateAdapterOfVacation = new EntityDeletionOrUpdateAdapter<Vacation>(__db) {
      @Override
      public String createQuery() {
        return "UPDATE OR ABORT `d308mobiledevelopmentapplicationandroid` SET `productID` = ?,`productName` = ?,`productPrice` = ? WHERE `productID` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, Vacation value) {
        stmt.bindLong(1, value.getVacationID());
        if (value.getVacationTitle() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getVacationTitle());
        }
        stmt.bindString(3, value.getVacationHotel());
        stmt.bindLong(4, value.getVacationID());
      }
    };
  }

  @Override
  public void insert(final Vacation vacation) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __insertionAdapterOfVacation.insert(vacation);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void delete(final Vacation vacation) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __deletionAdapterOfVacation.handle(vacation);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void update(final Vacation vacation) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __updateAdapterOfVacation.handle(vacation);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public List<Vacation> getAllVacations() {
    final String _sql = "SELECT * FROM VACATIONS ORDER BY productID ASC";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfVacationID = CursorUtil.getColumnIndexOrThrow(_cursor, "vacationID");
      final int _cursorIndexOfVacationName = CursorUtil.getColumnIndexOrThrow(_cursor, "vacationName");
      final int _cursorIndexOfVacationHotel = CursorUtil.getColumnIndexOrThrow(_cursor, "vacationPrice");
      final List<Vacation> _result = new ArrayList<Vacation>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final Vacation _item;
        _item = new Vacation();
        final int _tmpVacationID;
        _tmpVacationID = _cursor.getInt(_cursorIndexOfVacationID);
        _item.setVacationID(_tmpVacationID);
        final String _tmpVacationName;
        if (_cursor.isNull(_cursorIndexOfVacationName)) {
          _tmpVacationName = null;
        } else {
          _tmpVacationName = _cursor.getString(_cursorIndexOfVacationName);
        }
        _item.setVacationTitle(_tmpVacationName);
        final String _tmpVacationHotel;
        _tmpVacationHotel = _cursor.getString(_cursorIndexOfVacationHotel);
        _item.setVacationHotel(_tmpVacationHotel);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  public static List<Class<?>> getRequiredConverters() {
    return Collections.emptyList();
  }
}
